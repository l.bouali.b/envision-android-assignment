package com.example.envisionocr.parser

import android.app.Activity
import android.content.Context
import android.util.Log
import com.example.envisionocr.baseActivity.Myapplication
import com.example.envisionocr.utitls.appUtils
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class FileParser {

    private var context: Context? = null
    private var activity: Activity? = null

    constructor(context: Context?, activity: Activity?) {
        this.context = context
        this.activity = activity
    }

    fun writeintofile(): Boolean {
        val utils: appUtils? = (activity!!.application as Myapplication).utils
        val dateFormat = SimpleDateFormat("dd:MM:yy_HH:mm")
        val date = Date()
        val dateformatted = dateFormat.format(date)
        val path = context!!.filesDir
        val file = File(path,  dateformatted+".txt")
        val text = utils?.getMessage()
        try {
            val stream = FileOutputStream(file)
            stream.write(text?.toByteArray())

        } catch (e: FileNotFoundException) {
            Log.i("tag","FileNotFoundException "+e.toString() )

            e.printStackTrace()
            return false
        } catch (e: IOException) {
            Log.i("tag","IOException "+ e.toString())

            e.printStackTrace()
            return false
        }
        return true
    }

    fun getFileName(): ArrayList<String>? {
        val path = context!!.filesDir
        val listfilenames = ArrayList<String>()
        val list = path.listFiles()
        for (i in list.indices) {
            listfilenames.add(list[i].name)
        }
        return listfilenames
    }
}


