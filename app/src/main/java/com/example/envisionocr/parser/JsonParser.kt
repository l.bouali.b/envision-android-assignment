package com.example.envisionocr.parser

import android.app.Activity
import android.util.Log
import com.example.envisionocr.baseActivity.Myapplication
import com.example.envisionocr.utitls.appUtils
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException

class JsonParser {

    private  var response: Response<ResponseBody?>
    private var activity: Activity? = null

    constructor(response: Response<ResponseBody?>, activity: Activity?) {
        this.response = response
        this.activity = activity
    }

    fun parseResponse(): Boolean {
        try {
            val utils: appUtils? = (activity!!.application as Myapplication).utils
            val valueResponse = response!!.body()!!.string()
            Log.i("tag","response"+valueResponse)

            val json = JSONObject(valueResponse)
            val jsonresponse = JSONObject(json["response"].toString())
            val jsonparagraphs = JSONArray(jsonresponse["paragraphs"].toString())
            for (i in 0 until jsonparagraphs.length()) {
                val jsonsingleparagraph = JSONObject(jsonparagraphs[i].toString())
                utils?.setMessage(jsonsingleparagraph["paragraph"].toString())
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            return false
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        return true
    }


}