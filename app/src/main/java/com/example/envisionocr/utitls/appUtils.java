package com.example.envisionocr.utitls;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

public class appUtils {
    private final Activity activity;
    private final Context context;
    private String message ;
    private static final int PERMISSION_REQUEST_CODE = 200;

    public appUtils(Activity activity, Context context) {
        this.activity=activity;
        this.context=context;
    }

    public appUtils() {
        activity = null;
        context = null;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message+=message;
    }
    public void showToast(String s ) {
        Toast.makeText(activity,s,Toast.LENGTH_SHORT).show();
    }


 }
