package com.example.envisionocr.view

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.envisionocr.parser.FileParser
import com.example.envisionocr.utitls.appUtils
import java.util.*

class FragmentLibrary : Fragment() {
    var listview: ListView? = null
    var listfilenames: ArrayList<String>? = null
    var appUtils: appUtils? = null


    companion object {
        fun newInstance( ): FragmentLibrary {
            val fragment = FragmentLibrary()
             return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(com.example.envisionocr.R.layout.fragment_library, container, false)
        listview = view.findViewById(com.example.envisionocr.R.id.listview) as ListView
        listfilenames = ArrayList()
        appUtils = appUtils(getActivity(), getContext())
        return view
    }

    override fun setMenuVisibility(visible: Boolean) {
        super.setMenuVisibility(visible)
        if (visible && isResumed()) {
            val fileParser = FileParser(context, activity)
            val listfiles: ArrayList<String>? = fileParser.getFileName()
            if (listfiles != null) {
                if (listfiles.isEmpty()) {
                    appUtils?.showToast("No file saved")
                } else {
                    val adapter = getContext()?.let {
                        listfiles?.let { it1 ->
                            ArrayAdapter(
                                    it,
                                     R.layout.activity_list_item,
                                    R.id.text1,
                                    it1
                            )
                        }
                    }
                    listview!!.adapter = adapter
                }
            }
        }
    }
}