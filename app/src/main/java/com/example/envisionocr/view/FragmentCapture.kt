package com.example.envisionocr.view

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.envisionocr.baseActivity.MainActivity
import com.example.envisionocr.baseActivity.Myapplication
import com.example.envisionocr.network.ApiManager
import com.example.envisionocr.network.ApiService
import com.example.envisionocr.parser.FileParser
import com.example.envisionocr.parser.JsonParser
import com.example.envisionocr.R
import com.example.envisionocr.utitls.appUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.fotoapparat.Fotoapparat
import io.fotoapparat.log.logcat
import io.fotoapparat.log.loggers
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.back
import io.fotoapparat.view.CameraView
import kotlinx.android.synthetic.main.fragment_capture.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*


class FragmentCapture : Fragment(), BottomNavigationView.OnNavigationItemSelectedListener {
    var fotoapparat: Fotoapparat? = null
    var fotoapparatState : FotoapparatState? = null
    private var mApiService: ApiService? = null
    var fileParser:FileParser ?=null
    private var mProgress: ProgressDialog? = null
    var button:Button ?= null
    lateinit var imageTotext: TextView
    lateinit var cameraView: CameraView
    lateinit var toolbar: BottomNavigationView

    companion object {
        fun newInstance(): FragmentCapture {
            val fragment = FragmentCapture()
             return fragment
        }
    }
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_capture, container, false)
        mProgress = ProgressDialog(activity)
        mProgress!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPrimaryDark)))
         cameraView = view?.findViewById<CameraView>(R.id.camera_view)
        button=view?.findViewById<Button>(R.id.fab_camera)
        imageTotext=view?.findViewById<TextView>(R.id.texttoshow)
        imageTotext.visibility=View.INVISIBLE
        mApiService = ApiManager.getClient()?.create(ApiService::class.java)
        toolbar = view.findViewById<View>(R.id.bottomnav) as BottomNavigationView
        toolbar.visibility=View.INVISIBLE
        toolbar.setOnNavigationItemSelectedListener(this)
        createFotoapparat(cameraView)
        fotoapparatState = FotoapparatState.OFF
        fotoapparat?.start()
        fotoapparatState = FotoapparatState.ON
        button?.setOnClickListener {
            Log.i("tag","button clicked")
            btnClick(button!!.getText().toString())

            takePhoto()
        }

        return view
    }
    override fun setMenuVisibility(visible: Boolean) {
        super.setMenuVisibility(visible)
        if (visible && isResumed) {
            button?.setText("CAPTURE")
            button?.setEnabled(true)
            button?.setBackgroundResource(R.drawable.roundedbutton)
            cameraView.setVisibility(View.VISIBLE)
            texttoshow.setVisibility(View.GONE)
            toolbar.visibility = View.GONE
            button?.setOnClickListener(View.OnClickListener {
                btnClick(button?.getText().toString()) })
        }
    }
    private fun takePhoto() {
        Log.i("tag","has permission")
        val photoResult = fotoapparat?.takePicture()
        val photoDir = File(activity!!.getExternalFilesDir("ImagesXcamera").toString() + "/")
        Log.i("tag", "file name :" + photoDir.name)
        if (!photoDir.exists()) photoDir.mkdir()
        val date = Date()
        val timestamp = date.time.toString()
        val photoFilePath = photoDir.absolutePath + "/" + timestamp + ".png"
        val photoFile = File(photoFilePath)
        photoResult?.saveToFile(photoFile)

         photoResult
                ?.toBitmap()
                ?.whenAvailable { bitmapPhoto ->
                    val savedUri = Uri.fromFile(photoFile)
                    Log.i("tag","path uri"+photoFile)
                    postImageToServer(savedUri)
                }
    }
    private fun createFotoapparat(cameraView: CameraView?) {

        fotoapparat = cameraView?.let {
            Fotoapparat(
                context = this!!.context!!,
                view = it,
                scaleType = ScaleType.CenterCrop,
                lensPosition = back(),
                logger = loggers(
                        logcat()
                ),
                cameraErrorCallback = { error ->
                 }
        )
        }
    }
    private fun postImageToServer(uri: Uri) {
        val ss1 = SpannableString("OCR in Progress")
        ss1.setSpan(ForegroundColorSpan(Color.WHITE), 0, ss1.length, 0)
        mProgress?.setMessage(ss1)
        mProgress?.show()
        mProgress?.setCancelable(false)
        mProgress?.setCanceledOnTouchOutside(false)
        val file = File(uri.path)
        val requestFile =
            RequestBody.create(MediaType.parse("image/png"), file)
        val body =
                MultipartBody.Part.createFormData("photo", file.name, requestFile)
        Log.i("tag","path requestFile "+file.name)

        val call = mApiService!!.uploadPhoto(body)
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val jsonParser = JsonParser(response, activity)
                      fileParser = FileParser(context, activity)

                    if (jsonParser.parseResponse()) {
                              setview(true)
                             mProgress?.dismiss()
                             button?.setOnClickListener {
                                 setview(false)
                                 btnClick(button!!.getText().toString())
                             }

                    } else {
                     }
                 }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                  mProgress?.dismiss()
                t.printStackTrace()
             }
        })
    }
    private fun setview(view: Boolean) {
        if (view) {
            button?.setText("SAVE TEXT TO LIBRARY")
            cameraView.visibility=View.INVISIBLE
            val utils: appUtils = (activity!!.application as Myapplication).utils!!
            imageTotext.visibility=View.VISIBLE
            imageTotext.setText(utils.message)
        }
    }
    private fun btnClick(event: String) {
        if (event != "CAPTURE") {
            button?.setBackgroundResource(R.drawable.roundedbuttongray)
            if (fileParser!!.writeintofile()) {
                toolbar.setVisibility(View.VISIBLE)
            } else {
//                appUtils.showToast("Failed writing into file")
            }
        } else {

        }
    }
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.getItemId()) {
            R.id.library -> {
                val intent = Intent(activity, MainActivity::class.java)
                startActivity(intent)
                 return true
            }
        }
        return false
    }
    enum class FotoapparatState{
        ON,OFF
    }

}