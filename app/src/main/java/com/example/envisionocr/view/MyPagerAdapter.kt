package com.example.envisionocr.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.envisionocr.view.FragmentLibrary.Companion.newInstance


class MyPagerAdapter(mFragmentManager: FragmentManager) :
    FragmentPagerAdapter(mFragmentManager) {
    private var mFragmentAtPos0: Fragment? = null
    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "CAPTURE"
            1 -> return "LIBRARY"
        }
        return "Tab $position"
    }

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
           FragmentCapture.newInstance()

        } else newInstance()
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItemPosition(`object`: Any): Int {
        return if (`object` is FragmentCapture && mFragmentAtPos0 is FragmentLibrary) PagerAdapter.POSITION_NONE else PagerAdapter.POSITION_UNCHANGED
    }



    companion object {
        private const val NUM_ITEMS = 2
    }

}


